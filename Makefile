
########################################################
# MANUAL MAKEFILE                                      #
# USE THIS FOR COMPILATION ON A COMMAND LINE INTERFACE #
########################################################

# Source directories separated by space
SRCDIR=src/
INCDIR=src/
OBJDIR=build/
BINDIR=dist/
TARGET=graphtest
CXX=g++

SRC=$(wildcard $(addsuffix *.cpp,$(SRCDIR)))
OBJ=$(addprefix $(OBJDIR), $(patsubst %.cpp, %.o, $(notdir $(SRC))))
VPATH=$(SRCDIR)

CXXFLAGS=-O2 -std=c++11

$(TARGET) : $(OBJ)
	@echo Linking...
	@mkdir -p $(BINDIR)
	@$(CXX) $(CXXFLAGS) -o $(BINDIR)$@ $(OBJ)
	@cp -R ./InputFile $(BINDIR)
	@echo Compilation complete.

$(OBJDIR)%.o : %.cpp
	@echo Compiling $< in $@...
	@mkdir -p $(OBJDIR)
	@$(CXX) $(CXXFLAGS) $(addprefix -I,$(INCDIR)) -c -o $@ $<

clean :
	@echo Cleaning...
	@$(RM) -r $(OBJDIR)
	@$(RM) -r $(BINDIR)
	@echo Cleaning complete.
