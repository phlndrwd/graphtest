#ifndef EDGE_H
#define EDGE_H

#include "Vertex.h"

class Vertex;

class Edge {
public:
    Edge(Vertex*);
    ~Edge();
    
    Vertex* GetVertex();
    
private:
    Vertex* mVertex;
};

#endif

