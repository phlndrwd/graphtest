#ifndef VERTEX_H
#define VERTEX_H

#include "Edge.h"

#include <vector>
#include <string>

class Edge;

class Vertex {
public:
    Vertex(std::string);
    ~Vertex();

    std::string& GetLabel();
    unsigned GetOutDegree();
    Edge* GetEdge(unsigned);
    Edge* GetRandomEdge();
    std::vector<Edge*>& GetEdges();
    void SetEdge(Vertex*);

private:
    std::vector<Edge*> mEdges;
    std::string mLabel;
};

#endif

