#include "Edge.h"

Edge::Edge(Vertex* vertex) {
    mVertex = vertex;
}

Edge::~Edge() {
}

Vertex* Edge::GetVertex() {
    return mVertex;
}
