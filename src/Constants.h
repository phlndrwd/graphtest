#ifndef CONSTANTS_H
#define CONSTANTS_H

namespace Constants {

    enum eVertexDefinitions {
        eRootVertex,
        eEdge,
        eConnectedVertex
    };
}

#endif

