#include "Constants.h"
#include "Graph.h"
#include <iostream>
#include <sstream>
#include <fstream>

#define RANDOM_WALK 1

std::vector<std::vector<std::string>> rawTextData;

const std::vector<std::string> StringToWords(const std::string& inputString, const char wordTerminationCharacter) {
    std::stringstream stringStream(inputString);

    std::string word = "";
    std::vector<std::string> wordList;

    while (std::getline(stringStream, word, wordTerminationCharacter)) {
        wordList.push_back(word);
    }
    return wordList;
}

bool ReadInputFile(std::string inputFile) {
    std::cout << "Reading file \"" << inputFile << "\"..." << std::endl;
    std::ifstream fileStream(inputFile.c_str(), std::ios::in);

    if (fileStream.is_open()) {
        std::string readLine;
        while (std::getline(fileStream, readLine)) {
            rawTextData.push_back(StringToWords(readLine, ' '));
        }
        fileStream.close();
        return true;
    } else std::cout << "File path \"" << inputFile << "\" is invalid." << std::endl;
    return false;
}

#if RANDOM_WALK 
const unsigned numberOfParams = 3;
bool validParams[numberOfParams] = {false};

int main(int numberOfArguments, char* arguments[]) {
    if (numberOfArguments != 4) {
        std::cerr << "ERROR> Incorrect number of input arguments. System exiting..." << std::endl;
        return 1;
    }

    std::string inputFile;
    unsigned paramLValue = 0;
    unsigned paramNValue = 0;

    for (unsigned i = 1; i < numberOfArguments; ++i) {
        std::string param = arguments[i];
        if (param[1] == '=') {
            if (param[0] == 'L') {
                paramLValue = std::stoi(param.substr(2));
                validParams[i - 1] = true;
            } else if (param[0] == 'N') {
                paramNValue = std::stoi(param.substr(2));
                validParams[i - 1] = true;
            }
        }
    }
    unsigned numberValid = 0;
    for (unsigned i = 0; i < numberOfParams; ++i) {
        if (validParams[i]) {
            numberValid += 1;
        } else inputFile = arguments[i + 1];   
    }
    if (numberValid < 2) {
        std::cerr << "ERROR> Too many unknown input arguments. System exiting..." << std::endl;
        return 1;
    }

    if (ReadInputFile(inputFile)) {
        Graph graph(rawTextData);
        graph.RandomWalk(paramLValue, paramNValue);
        return 0;
    }
}
#else

int main(int numberOfArguments, char* arguments[]) {
    if (numberOfArguments != 2) {
        std::cerr << "ERROR> Incorrect number of input arguments. System exiting..." << std::endl;
        return 1;
    }

    if (ReadInputFile(arguments[1])) {
        Graph graph(rawTextData);
        graph.MinMaxOutDegree();
        graph.MinMaxInDegree();
        return 0;
    }
}
#endif
