#ifndef GRAPH_H
#define GRAPH_H

#include "Vertex.h"

#include <vector>
#include <string>
#include <map>

class Graph {
public:
    Graph(std::vector<std::vector<std::string>>&);
    ~Graph();

    void MinMaxOutDegree();
    void MinMaxInDegree();
    void RandomWalk(unsigned, unsigned);

private:
    void Initialise(std::vector<std::vector<std::string>>&);
    Vertex* AddVertex(std::string&);

    std::vector<Vertex*> mVertices;
    std::map<std::string, unsigned> mInDegrees;
};

#endif

