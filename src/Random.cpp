#include "Random.h"

Random::Random(unsigned seed) {
    if (seed == 1) mSeed = mRandomDevice();
    else mSeed = seed;
    mEngine.seed(mSeed);
}

int Random::GetUniformInt(const int minimum, const int maximum) {
    std::uniform_int_distribution< >::param_type customParams{ minimum, maximum};
    mUniformIntDistribution.param(customParams);

    return mUniformIntDistribution(mEngine);
}

unsigned Random::GetSeed() {
    return mSeed;
}

