#ifndef RANDOM_H
#define RANDOM_H

#include <random>
#include "Constants.h"

class Random {
public:

    static Random& Get() {
        static Random random;
        return random;
    }

    int GetUniformInt(const int, const int);
    unsigned GetSeed();

    Random(Random const&) = delete;
    void operator=(Random const&) = delete;
private:
    Random(unsigned seed = 1);

    std::random_device mRandomDevice;
    std::default_random_engine mEngine;
    std::uniform_int_distribution< int > mUniformIntDistribution;

    unsigned mSeed;
};

#endif

