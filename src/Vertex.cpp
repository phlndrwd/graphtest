#include "Vertex.h"
#include "Random.h"

Vertex::Vertex(std::string label) {
    mLabel = label;
}

Vertex::~Vertex() {
    for (unsigned i = 0; i < mEdges.size(); ++i) delete mEdges[i];
}

std::string& Vertex::GetLabel() {
    return mLabel;
}

unsigned Vertex::GetOutDegree() {
    return mEdges.size();
}

Edge* Vertex::GetEdge(unsigned index) {
    return mEdges[index];
}

Edge* Vertex::GetRandomEdge() {
    return GetEdge(Random::Get().GetUniformInt(0, mEdges.size() - 1));
}

std::vector<Edge*>& Vertex::GetEdges() {
    return mEdges;
}

void Vertex::SetEdge(Vertex* connectedVertex) {
    mEdges.push_back(new Edge(connectedVertex));
}