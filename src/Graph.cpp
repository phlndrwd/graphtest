#include "Graph.h"
#include "Constants.h"
#include "Random.h"

#include <iostream>
#include <limits>

Graph::Graph(std::vector<std::vector<std::string>>&rawTextData) {
    Initialise(rawTextData);
}

Graph::~Graph() {
    for (unsigned i = 0; i < mVertices.size(); ++i) {
        delete mVertices[i];
    }
}

void Graph::Initialise(std::vector<std::vector<std::string> >& rawTextData) {
    for (unsigned i = 0; i < rawTextData.size(); ++i) {
        Vertex* firstVertex = AddVertex(rawTextData[i][Constants::eRootVertex]);
        firstVertex->SetEdge(AddVertex(rawTextData[i][Constants::eConnectedVertex]));
    }
}

Vertex* Graph::AddVertex(std::string& vertexLabel) {
    Vertex* vertex = NULL;
    for (unsigned i = 0; i < mVertices.size(); ++i) {
        if (mVertices[i]->GetLabel() == vertexLabel) {
            return mVertices[i];
        }
    }
    vertex = new Vertex(vertexLabel);
    mVertices.push_back(vertex);

    return vertex;
}

void Graph::MinMaxOutDegree() {
    unsigned min = std::numeric_limits<unsigned int>::max();
    unsigned max = 0;
    for (unsigned i = 0; i < mVertices.size(); ++i) {
        unsigned outDegree = mVertices[i]->GetOutDegree();
        if (outDegree < min) min = outDegree;
        if (outDegree > max) max = outDegree;
    }
    std::cout << "Out-degree minimum> " << min << ", maximum> " << max << std::endl;
}

void Graph::MinMaxInDegree() {
    for (unsigned i = 0; i < mVertices.size(); ++i) {
        mInDegrees[mVertices[i]->GetLabel()] = 0;
    }
    for (unsigned i = 0; i < mVertices.size(); ++i) {
        std::vector<Edge*>& edges = mVertices[i]->GetEdges();
        for (unsigned j = 0; j < edges.size(); ++j) {
            mInDegrees[edges[j]->GetVertex()->GetLabel()] += 1;
        }
    }
    unsigned min = std::numeric_limits<unsigned int>::max();
    unsigned max = 0;
    for (std::map<std::string, unsigned>::iterator it = mInDegrees.begin(); it != mInDegrees.end(); ++it) {
        unsigned inDegree = it->second;
        if (inDegree < min) min = inDegree;
        if (inDegree > max) max = inDegree;
    }
    std::cout << "In-degree minimum> " << min << ", maximum> " << max << std::endl;
}

void Graph::RandomWalk(unsigned L, unsigned N) {
    std::cout << "Random walk with N=" << N << " and L=" << L << std::endl;
    for (unsigned n = 0; n < N; ++n) {
        Vertex* randomVertex = mVertices[Random::Get().GetUniformInt(0, mVertices.size() - 1)];
        Vertex* previousRandomVertex = NULL;
        std::cout << randomVertex->GetLabel() << ",";
        for (unsigned l = 0; l < L - 1; ++l) {
            previousRandomVertex = randomVertex;
            randomVertex = previousRandomVertex->GetRandomEdge()->GetVertex();
            std::cout << randomVertex->GetLabel();
            if(l < L - 2) std::cout << ",";
        }
        std::cout << std::endl;
    }
}
